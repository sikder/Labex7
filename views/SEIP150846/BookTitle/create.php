<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

//echo Message::getMessage();
echo Message::message();
?>

<!DOCTYPE html>
<html lang="en">

<head>


    <title>Book Title</title>

    <!-- SIDEBAR -->
    

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/BookTitle_assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/BookTitle_assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/BookTitle_assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/BookTitle_assets/css/style.css">
    <link rel="stylesheet" href="../../../resource/BookTitle_assets/css/main.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/BookTitle_assets/js/html5shiv.js"></script>
    <script src="../../../resource/BookTitle_assets/js/respond.min.js"></script>
    <![endif]-->
    
    
    <!-- Javascript -->
    <script src="../../../resource/BookTitle_assets/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/BookTitle_assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/BookTitle_assets/js/jquery.backstretch.min.js"></script>
    <script src="../../../resource/BookTitle_assets/js/scripts.js"></script>
    
    
    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../resource/BookTitle_assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/BookTitle_assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/BookTitle_assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/BookTitle_assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../resource/BookTitle_assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>
<header style="height: 60px;background-color: midnightblue;text-align: center;margin-top: -10px;">
    <h2 style="color: white">Atomic Project</h2>
</header>
<!-- sidebar start -->

<div id="wrapper">
    <aside id="sideBar">
        <ul class="main-nav">
            <!-- Your Logo Or Site Name -->
            <!--<li class="nav-brand">
                <a href="#"><img src="../../../resource/BookTitle_assets/img/logo.png" alt=""></a>
            </li>-->
            <!--<li class="main-search">
                <form action="#">
                    <input type="text" class="form-control search-input" placeholder="Search here...">
                    <i class="fa fa-search"></i>
                </form>
            </li>-->
            <li>
                <a href="create.php">- Book Title</a>
            </li>
            <li>
                <a href="../Birthday/create.php">- Birthday</a>
            </li>
            <li>
                <a href="../City/create.php">- City</a>
            </li>
            <li>
                <a href="../Email/create.php">- Email</a>
            </li>
            <li>
                <a href="#">- Gender</a>
            </li>
            <li>
                <a href="#">- Hobbies</a>
            </li>
            <li>
                <a href="#">- Profile Picture</a>
            </li>
            <li>
                <a href="#">- Summary of Organization</a>
            </li>
        </ul>
    </aside>
</div>

<!-- sidebar end -->


<!-- Top content -->
<div class="top-content">
    <div class="inner-bg">
        <div class="container">
            <!--<div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong>Bootstrap</strong> Login Form</h1>
                    <div class="description">
                        <p>
                            This is a free responsive login form made with Bootstrap.
                            Download it on <a href="http://azmind.com"><strong>AZMIND</strong></a>, customize and use it as you like!
                        </p>
                    </div>
                </div>
            </div>-->
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Details of BookTitle</h3>
                            <p>Enter your book name and author name:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-book" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="store.php" method="post" class="login-form">
                            <div class="form-group">
                                <label class="sr-only" for="book_name">Username</label>
                                <input type="text" name="book_name" placeholder="Book name....." class="form-username form-control" id="book_name">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="author_name">Password</label>
                                <input type="text" name="author_name" placeholder="Author name....." class="form-password form-control" id="author_name">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="price">Password</label>
                                <input type="text" name="price" placeholder="Price....." class="form-password form-control" id="price">
                            </div>
                            <button type="submit" class="btn">enter</button>
                        </form>
                    </div>
                </div>
            </div>
            <!--<div class="row">
                <div class="col-sm-6 col-sm-offset-3 social-login">
                    <h3>...or login with:</h3>
                    <div class="social-login-buttons">
                        <a class="btn btn-link-1 btn-link-1-facebook" href="#">
                            <i class="fa fa-facebook"></i> Facebook
                        </a>
                        <a class="btn btn-link-1 btn-link-1-twitter" href="#">
                            <i class="fa fa-twitter"></i> Twitter
                        </a>
                        <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                            <i class="fa fa-google-plus"></i> Google Plus
                        </a>
                    </div>
                </div>
            </div>-->
        </div>
    </div>

</div>

</body>

</html>