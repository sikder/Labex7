<?php
require_once("../../../vendor/autoload.php");
use App\Summary_Of_Organization\Summary_Of_Organization;

$objOrganization = new Summary_Of_Organization();
$objOrganization->setData($_POST);
$objOrganization->store();