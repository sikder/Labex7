<?php
namespace App\Birthday;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Birthday extends DB{
    public $id;
    public $user_name;
    public $birth_date;

    public function __construct(){
        parent::__construct();
    }
    public function setData($postVariableData=NULL){
        /*$this->book_title = $postVariableData['book_name'];
        $this->author_name = $postVariableData['author_name'];
        $this->price = $postVariableData['price'];*/
        if(array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("user_name",$postVariableData)){
            $this->user_name = $postVariableData['user_name'];
        }
        if(array_key_exists("birth_date",$postVariableData)){
            $this->birth_date = $postVariableData['birth_date'];
        }
    }

    public function store(){
        /*$sql = "insert into birthday(name,birth_date)
                VALUES ('$this->user_name', '$this->birth_date')";

        echo $sql;
        die();*/

        $arrData = array($this->user_name,$this->birth_date);
        $sql = "insert into birthday(name,birth_date)
                VALUES (?, ?)";
        $STH = $this->DBH->prepare($sql);  //prepare() object return kore
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result){
            //Message::setMessage("data has been inserted succesfully");
            Message::message("data has been inserted succesfully");
        }else {
            //Message::setMessage("Failed! data has not been inserted");
            Message::message("Failed! data has not been inserted");
        }
        Utility::redirect('create.php');
        //header('Location:create.php');
    }
}