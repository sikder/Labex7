<?php
namespace App\Profile_Picture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class Profile_Picture extends DB{
    public $id;
    public $name;
    public $image_name;

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariableData=NULL){
        if(array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("name",$postVariableData)){
            $this->name = $postVariableData['name'];
        }
        if(array_key_exists("image_name",$postVariableData)){
            $this->image_name = $postVariableData['image_name'];
        }
    }

    public function store(){
        /*$sql = "insert into profile_picture(name,profile_picture)
                VALUES (' $this->name','$this->image_name')";

        echo $sql;

        die();*/

        $arrData = array($this->name,$this->image_name);
        $sql = "insert into profile_image(name,profile_picture)
                VALUES (?, ?)";
        $STH = $this->DBH->prepare($sql);  //prepare() object return kore
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result){
            //Message::setMessage("data has been inserted succesfully");
            Message::message("data has been inserted succesfully");
        }else {
            //Message::setMessage("Failed! data has not been inserted");
            Message::message("Failed! data has not been inserted");
        }
        Utility::redirect('create.php');
        //header('Location:create.php');
    }

    public function index(){
        echo "I am inside the index method of Profile_Picture class<br>";
    }
}
