<?php
namespace App\Summary_Of_Organization;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class Summary_Of_Organization extends DB{
    public $id;
    public $short_name;
    public $full_name;
    public $task;

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariableData=NULL){
        if(array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("short_name",$postVariableData)){
            $this->short_name = $postVariableData['short_name'];
        }
        if(array_key_exists("full_name",$postVariableData)){
            $this->full_name = $postVariableData['full_name'];
        }
        if(array_key_exists("task",$postVariableData)){
            $this->task = $postVariableData['task'];
        }
    }

    public function store(){
        $arrData = array($this->short_name,$this->full_name,$this->task);
        $sql = "insert into summary_of_organization(short_name,,full_name,task)
                VALUES (?, ?, ?)";
        $STH = $this->DBH->prepare($sql);  //prepare() object return kore
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result){
            //Message::setMessage("data has been inserted succesfully");
            Message::message("data has been inserted succesfully");
        }else {
            //Message::setMessage("Failed! data has not been inserted");
            Message::message("Failed! data has not been inserted");
        }
        Utility::redirect('create.php');
        //header('Location:create.php');
    }

    public function index(){
        echo "I am inside the index method of Summary_Of_Organization class<br>";
    }
}

