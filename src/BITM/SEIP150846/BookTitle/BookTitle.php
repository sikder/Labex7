<?php
namespace App\BookTitle;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB{
    public $id;
    public $book_title;
    public $author_name;
    public $price;

    public function __construct(){
        parent::__construct();
    }
    public function setData($postVariableData=NULL){
        /*$this->book_title = $postVariableData['book_name'];
        $this->author_name = $postVariableData['author_name'];
        $this->price = $postVariableData['price'];*/
        if(array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }
        if(array_key_exists("book_title",$postVariableData)){
            $this->book_title = $postVariableData['book_title'];
        }
        if(array_key_exists("author_name",$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
        if(array_key_exists("price",$postVariableData)) {
            $this->price = $postVariableData['price'];
        }
    }

    public function store(){
        $arrData = array($this->book_title,$this->author_name,$this->price);
        $sql = "insert into book_title(book_title,author_name,price)
                VALUES (?, ?, ?)";
        $STH = $this->DBH->prepare($sql);  //prepare() object return kore
        $result = $STH->execute($arrData);
        //Utility::dd($result);      // arporer codegula run hbe na
        if($result){
            //Message::setMessage("data has been inserted succesfully");
            Message::message("data has been inserted succesfully");
        }else {
            //Message::setMessage("Failed! data has not been inserted");
            Message::message("Failed! data has not been inserted");
        }
        Utility::redirect('new.php');
        //header('Location:create.php');
    }
}